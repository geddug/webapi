<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->dotaApi="http://www.dota2.com/webapi/ILeaderboard/GetDivisionLeaderboard/v0001?division=";
		$this->sholatApi = "https://time.siswadi.com/pray/";
		$this->quran = "http://api.alquran.cloud/";
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if($_POST != NULL) {
			$kota = $this->input->post('kota');
			$h = $this->curl->simple_get($this->sholatApi.$kota);
			$data['waktu'] = json_decode($h);
			$this->load->view('modal/waktu', $data);
		} else {
			$this->load->view('template/header');
			$this->load->view('index');
			$this->load->view('template/footer');
		}
	}
	public function dota() {
		/*$americas = $this->curl->simple_get($this->dotaApi."americas");
		$data['americas'] = json_decode($americas);
		$europe = $this->curl->simple_get($this->dotaApi."europe");
		$data['europe'] = json_decode($europe);*/
		$se_asia = $this->curl->simple_get($this->dotaApi."se_asia");
		$data['se_asia'] = json_decode($se_asia);
		//$china = $this->curl->simple_get($this->dotaApi."china");
		//$data['china'] = json_decode($china);
		$this->load->view('template/header');
		$this->load->view('dota', $data);
		$this->load->view('template/footer');
	}
	public function quran() {
		if($_POST != NULL) {
			$id = $this->input->post('surah');
			$q = $this->curl->simple_get($this->quran."surah/".$id);
			$data['quran'] = json_decode($q);
			$this->load->view('modal/quran', $data);
		} else {
			$q = $this->curl->simple_get($this->quran."surah");
			$data['quran'] = json_decode($q);
			$this->load->view('template/header');
			$this->load->view('quran', $data);
			$this->load->view('template/footer');
		}
	}
}
