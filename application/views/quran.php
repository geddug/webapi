<div class="wrapper">
   <div class="container">
      <!-- Page-Title -->
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-title">Al-Qur'an</h4>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="card-box table-responsive">
               <div class="">
                  <form id="formcari" class="form-inline">
                     <select class="form-control select2" id="surah" name="surah" required>
                        <option value="">Select</option>
                        <?php foreach ($quran->data as $key) { ?>
                        <option value="<?php echo $key->number;?>"><?php echo $key->englishName;?></option>
                        <?php } ?>
                     </select>
                  </form>
               </div>
               <div id="ganti">
                  <table class="table">
                     <thead>
                        <tr>
                           <th></th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td></td>
                           <td></td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <!-- end col -->
      </div>
      <!-- end row -->
      <!-- Footer -->
      <footer class="footer text-right">
         <div class="container">
            <div class="row">
               <div class="col-xs-6">
                  <?php echo date("Y");?> © Yaha.
               </div>
            </div>
         </div>
      </footer>
      <!-- End Footer -->
   </div>
   <!-- end container -->
</div>
<script>
$(".select2").select2();$("#surah").on("change",function(){var a=$("#surah").val();if(a!=""){$("#modalloading").modal({backdrop:"static",keyboard:false},"show");$.ajax({url:"<?php echo base_url();?>quran",type:"post",data:$("#formcari").serialize(),success:function(b){$("#ganti").html(b);$("#modalloading").modal("hide")}})}});
</script>