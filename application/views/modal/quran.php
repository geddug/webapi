<table class="table table-striped">
	<thead>
		<tr>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Surah Ke</td>
			<td><?php echo $quran->data->number;?></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td><b><?php echo $quran->data->name;?></b></td>
		</tr>
		<tr>
			<td>Jumlah Ayat</td>
			<td><?php echo $quran->data->numberOfAyahs;?></td>
		</tr>
	</tbody>
</table>
<table class="table table-striped">
	<thead>
		<tr>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php $n=1; foreach ($quran->data->ayahs as $key) { ?>
		<tr>
			<td><?php echo $n;?></td>
			<td align="right"><b><?php print_r($key->text); ?></b></td>
		</tr>
	<?php $n++; } ?>
	</tbody>
</table>