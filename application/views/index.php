<div class="wrapper">
   <div class="container">
      <!-- Page-Title -->
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-title">Jadwal sholat</h4>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="card-box table-responsive">
               <div class="pull-left">
                  <form id="formcari" class="form-inline">
                     <input type="text" name="kota" class="form-control" placeholder="Masukkan Nama Kota" required>
                     <button type="submit" class="btn btn-primary">Cari</button>
                  </form>
               </div>
               <div id="ganti">
                  <table class="table">
                     <thead>
                        <tr>
                           <th></th>
                           <th>Waktu</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td></td>
                           <td></td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <!-- end col -->
      </div>
      <!-- end row -->
      <!-- Footer -->
      <footer class="footer text-right">
         <div class="container">
            <div class="row">
               <div class="col-xs-6">
                  <?php echo date("Y");?> © Yaha.
               </div>
            </div>
         </div>
      </footer>
      <!-- End Footer -->
   </div>
   <!-- end container -->
</div>
<script>
$("#formcari").on("submit",(function(a){a.preventDefault();$("#modalloading").modal({backdrop:"static",keyboard:false},"show");$.ajax({url:"",type:"post",data:$("#formcari").serialize(),success:function(b){$("#ganti").html(b);$(".modal").modal("hide")},})}));
</script>