
        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Rank Dota 2</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

							<ul class="nav nav-tabs">
                           <!--<li role="presentation" class="active">
                              <a href="#americas" role="tab" data-toggle="tab">
								Americas <span class="badge badge-primary"></span>
							  </a>
                           </li>
                           <li role="presentation">
                              <a href="#europe" role="tab" data-toggle="tab">
								Europe <span class="badge badge-success"></span>
							  </a>
                           </li>-->
						   <li role="presentation">
                              <a href="#se_asia" role="tab" data-toggle="tab">
								SE Asia <span class="badge badge-success"></span>
							  </a>
                           </li>
						   <!--<li role="presentation">
                              <a href="#china" role="tab" data-toggle="tab">
								China <span class="badge badge-success"></span>
							  </a>
                           </li>-->
                        </ul>
                        <div class="tab-content">
                           <!--<div role="tabpanel" class="tab-pane fade in active" id="americas">
                              <table id="datatable" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Rank</th>
                                       <th>Name</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $no=1; foreach ($americas->leaderboard as $key) { ?>
                                    <tr>
                                        <td><?php echo $no;?></td>
                                        <td><?php echo $key->name;?></td>
                                    </tr>
								<?php $no++; } ?>
                                 </tbody>
                              </table>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="europe">
                              <table id="datatable1" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Rank</th>
                                       <th>Name</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $no=1; foreach ($europe->leaderboard as $key) { ?>
                                    <tr>
                                        <td><?php echo $no;?></td>
                                        <td><?php echo $key->name;?></td>
                                    </tr>
								<?php $no++; } ?>
                                 </tbody>
                              </table>
                           </div>-->
						   <div role="tabpanel" class="tab-pane fade in active" id="se_asia">
                              <table id="datatable2" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Rank</th>
                                       <th>Name</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $no=1; foreach ($se_asia->leaderboard as $key) { ?>
                                    <tr>
                                        <td><?php echo $no;?></td>
                                        <td><?php echo $key->name;?></td>
                                    </tr>
								<?php $no++; } ?>
                                 </tbody>
                              </table>
                           </div>
						   <!--<div role="tabpanel" class="tab-pane fade" id="china">
                              <table id="datatable3" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                       <th>Rank</th>
                                       <th>Name</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $no=1; foreach ($china->leaderboard as $key) { ?>
                                    <tr>
                                        <td><?php echo $no;?></td>
                                        <td><?php echo $key->name;?></td>
                                    </tr>
								<?php $no++; } ?>
                                 </tbody>
                              </table>
                           </div>-->

                        </div>
							
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                <?php echo date("Y");?> © Yaha.
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

        </div>
